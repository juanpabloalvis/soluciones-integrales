package com.integralsolutions.account.service;

import com.integralsolutions.account.model.Item;

import java.util.List;

public interface ItemsService {
    void addItem(Item i);
    void updateItem(Item i);
    List<Item> listItems();
    Item getItemById(int id);
    void removeItem(int id);
}
