package com.integralsolutions.account.service;

import com.integralsolutions.account.model.User;

public interface UserService {
    void save(User user);

    User findByUsername(String username);
}
