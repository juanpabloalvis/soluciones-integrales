package com.integralsolutions.account.service;

import com.integralsolutions.account.model.Item;
import com.integralsolutions.account.repository.ItemsRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class ItemsServiceImpl implements ItemsService {

    @Autowired
    ItemsRepository itemsRepository;

    @Override
    public void addItem(Item i) {

    }

    @Override
    public void updateItem(Item i) {

    }

    @Override
    public List<Item> listItems() {
        return itemsRepository.findAll();
    }

    @Override
    public Item getItemById(int id) {
        return null;
    }

    @Override
    public void removeItem(int id) {

    }
}
