package com.integralsolutions.account.repository;

import com.integralsolutions.account.model.Item;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ItemsRepository extends JpaRepository<Item, Long> {
    Item findByname(String name);
}
