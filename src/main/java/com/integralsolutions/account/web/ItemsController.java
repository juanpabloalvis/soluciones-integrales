package com.integralsolutions.account.web;

import com.integralsolutions.account.repository.ItemsRepository;
import com.integralsolutions.account.service.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ItemsController {
    @Autowired
    private SecurityService securityService;

    @Autowired
    private ItemsRepository itemsRepository;

    @RequestMapping(value = "/items", method = RequestMethod.GET)
    public String getItems(Model model) {

        model.addAttribute("allItems", itemsRepository.findAll());
        return "items";
    }
}
