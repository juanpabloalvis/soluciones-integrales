-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.7.12-log - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura de base de datos para integralsolutions
CREATE DATABASE IF NOT EXISTS `integralsolutions` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `integralsolutions`;


-- Volcando estructura para tabla integralsolutions.costumer_type
DROP TABLE IF EXISTS `costumer_type`;
CREATE TABLE IF NOT EXISTS `costumer_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla integralsolutions.costumer_type: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `costumer_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `costumer_type` ENABLE KEYS */;


-- Volcando estructura para tabla integralsolutions.customer
DROP TABLE IF EXISTS `customer`;
CREATE TABLE IF NOT EXISTS `customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `document_id` varchar(20) DEFAULT NULL,
  `name` varchar(80) DEFAULT NULL,
  `address` varchar(150) DEFAULT NULL,
  `phone_number` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla integralsolutions.customer: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;


-- Volcando estructura para tabla integralsolutions.item
DROP TABLE IF EXISTS `item`;
CREATE TABLE IF NOT EXISTS `item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `value` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla integralsolutions.item: ~34 rows (aproximadamente)
/*!40000 ALTER TABLE `item` DISABLE KEYS */;
INSERT IGNORE INTO `item` (`id`, `name`, `value`) VALUES
	(1, 'Balance General para donacion ', 40000),
	(2, 'Estados Financieros 01 año', 68000),
	(3, 'Estados Financieros 02 años comparativo', 95000),
	(4, 'Estados Financieros 03 años comparativo', 116000),
	(5, 'Estados Financieros incompletos con notas para contratacion (NIIF)', 225000),
	(6, 'Estados Financieros completos con notas para contratacion (BAJO NIIF)', 600000),
	(7, 'Flujo de efectivo a 05 años', 100000),
	(8, 'Certificado de Ingresos ', 32000),
	(9, 'Digitalizacion y envio por internet Estados Financieros o certificados o documentos que elaboramos oficina.', 5000),
	(10, 'Acta de no declarante (diligenciaientos formularios sencillos)', 13000),
	(11, 'Certificado de Ingresos y rete fuente empleados', 13000),
	(12, 'Diligenciamiento de camara de comercio 02 formularios ', 25000),
	(13, 'Diligenciamiento de formulario banco sencillo ', 20000),
	(14, 'Elaboracion de liquidacion de un año sin extras ', 38000),
	(15, 'Elaboracion de IVA minima sin contabilidad ', 250000),
	(16, 'Elaboracion de Rete Fuente sin contabilidad  (INC)', 140000),
	(17, 'Tramite de presentacion personal DIAN ', 100000),
	(18, 'Actualizacion de RUT por Internet ', 20000),
	(19, 'Impresión de RUT Oficina', 3000),
	(20, 'Rescate o Creación de Clave RUT Ofiicna', 12000),
	(21, 'Elaboracion y diligenciamiento de Industria y Comercio Municipios ', 28000),
	(22, 'Generacion  PILA (Individual)', 15000),
	(23, 'Generacion  PILA (Nomina) ($1.000) Cada Uno', 30000),
	(24, 'Generacion  Nomina 2018 (Formato digital)', 35000),
	(25, 'Presentacion de impuesto por Internet Particulares ', 25000),
	(26, 'Elaboracion de resolucion de facturacion', 25000),
	(27, 'Elaboracion Calendario Tributario Personal', 10000),
	(28, 'Contrato de Arrendamiento Bien Inmueble (TP01)', 60000),
	(29, 'Contrato de Trabajo laboral (TP02) ', 45000),
	(30, 'Elaboracion Promesa de Compraventa (TP04)', 120000),
	(31, 'Elaboracion Actas de Asamblea (TP03)', 70000),
	(32, 'Elaboracion Registro Unico de Proponentes (RUP)', 850000),
	(33, 'Cosntitucion de Persona Juridica (SAS)', 350000),
	(34, 'Arrendamiento uso de pagina WEB para no clientes  año ', 270000);
/*!40000 ALTER TABLE `item` ENABLE KEYS */;


-- Volcando estructura para tabla integralsolutions.itemp_group
DROP TABLE IF EXISTS `itemp_group`;
CREATE TABLE IF NOT EXISTS `itemp_group` (
  `item_id` int(11) DEFAULT NULL,
  `secuencia_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla integralsolutions.itemp_group: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `itemp_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `itemp_group` ENABLE KEYS */;


-- Volcando estructura para tabla integralsolutions.receipt
DROP TABLE IF EXISTS `receipt`;
CREATE TABLE IF NOT EXISTS `receipt` (
  `receipt_id` int(11) NOT NULL AUTO_INCREMENT,
  `date` int(11) NOT NULL DEFAULT '0',
  `total_value` int(11) NOT NULL DEFAULT '0',
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `item_group` int(11) NOT NULL DEFAULT '0',
  `service_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`receipt_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla integralsolutions.receipt: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `receipt` DISABLE KEYS */;
/*!40000 ALTER TABLE `receipt` ENABLE KEYS */;


-- Volcando estructura para tabla integralsolutions.role
DROP TABLE IF EXISTS `role`;
CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla integralsolutions.role: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT IGNORE INTO `role` (`id`, `name`) VALUES
	(1, 'ROLE_USER'),
	(2, 'ROLE_ADMIN');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;


-- Volcando estructura para tabla integralsolutions.user
DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla integralsolutions.user: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT IGNORE INTO `user` (`id`, `username`, `password`) VALUES
	(4, '11111111', '$2a$11$vg6GxaVrgXAQ0vYMVpRArO2x6qMsQVdVSHz8a43HNpFrQmAtgDIiq'),
	(5, '22222222', '$2a$11$IL9SLzC9/X3l5q6DlErvOexjyZ0Aplu3LsKq65oXGR9ddAt3o9ZDO');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;


-- Volcando estructura para tabla integralsolutions.user_role
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE IF NOT EXISTS `user_role`(
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `fk_user_role_roleid_idx` (`role_id`),
  CONSTRAINT `fk_user_role_roleid` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_user_role_userid` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla integralsolutions.user_role: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT IGNORE INTO `user_role` (`user_id`, `role_id`) VALUES
	(4, 1),
	(5, 1),
	(5, 2);
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
